package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

var logger = log.New(os.Stdout, "mabidiscordbot ", log.LstdFlags|log.Lshortfile)

func main() {
	d, err := startBot()
	if err != nil {
		logger.Fatalln(err)
	}
	defer d.Close()

	// mirror guild chat
	chatCh, hornCh := make(chan *chatMsg, 1), make(chan *chatMsg, 1)
	if conf.Discord.ChannelIdForMirroringGuildChat != "" {
		go chatPacketReaderLoop(chatCh)
	}
	if conf.Discord.ChannelIdForMirroringHorn != "" {
		go gamePacketReaderLoop(hornCh)
	}

	// wait ctrl + c or signal
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

	for {
		select {
		case <-sc:
			return
		case msg := <-chatCh:
			// 지금은 길드챗만 채널로 넘어옴
			if err := msg.Send(d, conf.Discord.ChannelIdForMirroringGuildChat); err != nil {
				logger.Println("error sending message: ", err)
			}

		case msg := <-hornCh:
			// 지금은 거뿔, 집뿔만 채널로 넘어옴
			if err := msg.Send(d, conf.Discord.ChannelIdForMirroringHorn); err != nil {
				logger.Println("error sending message: ", err)
			}
		}
	}
}

func init() {
	if len(os.Args) > 1 {
		if os.Args[1] == "nic" {
			printNicList()
			os.Exit(0)
		}
	}
}

func startBot() (*discordgo.Session, error) {
	d, err := discordgo.New("Bot " + conf.Discord.BotToken)
	if err != nil {
		return nil, err
	}

	d.Identify.Intents = discordgo.IntentGuildMessageTyping | discordgo.IntentGuildMessages | discordgo.IntentGuilds |
		discordgo.IntentDirectMessages | discordgo.IntentDirectMessageTyping

	if err := d.Open(); err != nil {
		return nil, err
	}

	d.AddHandler(handlerMessage)

	return d, nil
}

func handlerMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if len(m.Content) == 0 || m.Content[0] != '!' {
		return
	}

	author := m.Author.Username
	if m.Member != nil && m.Member.Nick != "" {
		author = m.Member.Nick
	}

	args := strings.Split(strings.TrimSpace(m.Content), " ")
	cmd, args := args[0][1:], args[1:]

	if existsDungeon(cmd) {
		handlerDungeonCommand(s, m.ChannelID, author, cmd, args)
	}

}

func handlerDungeonCommand(s *discordgo.Session, channelId string, author string, cmd string, args []string) {
	dungeon := dungeonMap[cmd]

	sendDefMsg := func() {
		msg := fmt.Sprintln(dungeon.Name, "릴수 {추가 파티 인원...}")
		if _, err := s.ChannelMessageSend(channelId, msg); err != nil {
			logger.Println("error sending message: ", err)
		}
	}

	if len(args) == 0 {
		sendDefMsg()
		return
	}

	count, err := strconv.Atoi(args[0])
	if err != nil {
		sendDefMsg()
		return
	}

	if count <= 0 {
		count = 1
	} else if count > 1000 {
		count = 1000
	}

	players := []string{author}
	players = append(players, args[1:]...)

	reward := dungeon.GetReward(players, count)

	if _, err := s.ChannelMessageSend(channelId, reward); err != nil {
		logger.Println("error sending message: ", err)
	}
}
