package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"sort"
)

type Dungeon struct {
	Name      string
	MaxPlayer int
	Rewards   dungeonRewardTable
}

type dungeonRewardTable []*dungeonRewardGroup

type dungeonRewardGroup struct {
	Probability float64
	Items       []*dungeonRewardItem
	// 한번 클리어 했을 때 파티원 중 한명만 보상을 받을 수 있게 한다.
	OnlyOnePlayer bool

	allWeight uint64
}

type dungeonRewardItem struct {
	Name   string
	Weight uint64

	calcedWeight uint64
}

type dungeonRewardContext struct {
	// adjProb            float64
	onlyOnePlayerFlags []bool
}

var _ json.Unmarshaler = (*dungeonJson)(nil)

type dungeonJson []*Dungeon

var dungeonMap = make(map[string]*Dungeon)

func loadDungeonJson() error {
	b, err := os.ReadFile("dungeon.json")
	if err != nil {
		return err
	}

	rawDungeon := dungeonJson(nil)

	if err := json.Unmarshal(b, &rawDungeon); err != nil {
		return err
	}

	for _, d := range rawDungeon {
		dungeonMap[d.Name] = d
	}

	return nil
}

func existsDungeon(name string) bool {
	_, ok := dungeonMap[name]
	return ok
}

func (t *Dungeon) GetReward(players []string, count int) string {
	nplayer := len(players)
	if nplayer > t.MaxPlayer {
		return fmt.Sprintln("인원이 너무 많아서 입장이 불가능합니다 ", t.MaxPlayer, "인 던전입니다.")
	}

	rewards := make([]map[string]int, nplayer)
	for i := range rewards {
		rewards[i] = make(map[string]int)
	}

	for i := 0; i < count; i++ {
		ctx := t.getDungeonRewardContext(nplayer)

		for pn := range players {
			for _, item := range t.Rewards.GetReward(ctx) {
				rewards[pn][item]++
			}
		}
	}

	s := fmt.Sprintln(t.Name, "던전", count, "릴")

	for pn, player := range players {
		rewardMap := rewards[pn]
		items := getKeys(rewardMap)

		sort.Slice(items, func(i, j int) bool {
			return rewardMap[items[i]] > rewardMap[items[j]]
		})

		s += fmt.Sprintln(player, "님의 보상")
		for _, item := range items {
			s += fmt.Sprintln("- ", item, " x ", rewardMap[item])
		}
		s += "\n"
	}

	return s
}

func (t *Dungeon) getDungeonRewardContext(nplayer int) *dungeonRewardContext {
	r := &dungeonRewardContext{
		onlyOnePlayerFlags: make([]bool, len(t.Rewards)),
		// adjProb:            float64(t.MaxPlayer) / float64(nplayer),
	}

	return r
}

func (t dungeonRewardTable) GetReward(ctx *dungeonRewardContext) []string {
	l := []string(nil)

	for i, group := range t {
		if group.OnlyOnePlayer && ctx.onlyOnePlayerFlags[i] {
			continue
		}

		// 그룹의 보상을 받을 확률
		groupProb := rand.Float64()
		if groupProb > group.Probability /* *ctx.adjProb */ {
			continue
		}

		ctx.onlyOnePlayerFlags[i] = true

		// 그룹의 아이템 중 어떤걸 받을까
		itemProb := uint64(rand.Int63n(int64(group.allWeight)))
		for _, item := range group.Items {
			if itemProb < item.calcedWeight {
				l = append(l, item.Name)
				break
			}
		}
	}

	return l
}

func (t *dungeonJson) UnmarshalJSON(b []byte) error {
	l := ([]*Dungeon)(nil)
	if err := json.Unmarshal(b, &l); err != nil {
		return err
	}

	for _, dungeon := range l {
		for _, reward := range dungeon.Rewards {
			allWeight := uint64(0)

			for _, item := range reward.Items {
				allWeight += item.Weight
				item.calcedWeight = allWeight
			}

			reward.allWeight = allWeight
		}
	}

	*t = dungeonJson(l)

	return nil
}
