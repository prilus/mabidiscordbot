package main

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	Discord discordConfig
	Pcap    pcapConfig
	Debug   bool
}

type discordConfig struct {
	BotToken                       string
	ChannelIdForMirroringGuildChat string
	ChannelIdForMirroringHorn      string
}

type pcapConfig struct {
	// 마비노기 클라이언트가 실행중인 ip, 없을 시 필터링하지 않는다
	ClientIp string
	NicName  string
}

var conf Config

func init() {
	confb, err := os.ReadFile("config.json")
	if err != nil {
		log.Fatalln("config.json read fail...", err)
	}

	if err := json.Unmarshal(confb, &conf); err != nil {
		log.Fatalln("config.json unmarshal fail...", err)
	}

	loadDungeonJson()
}
