module gitlab.com/prilus/mabidiscordbot

go 1.19

require (
	github.com/bwmarrin/discordgo v0.27.1
	github.com/google/gopacket v1.1.19
	golang.org/x/exp v0.0.0-20230420155640-133eef4313cb
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.1.0 // indirect
)
