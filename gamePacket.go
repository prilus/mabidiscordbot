// referenced from Aura Project
// Copyright (c) Aura development team - Licensed under GNU GPL

package main

import (
	"encoding/binary"
	"fmt"
	"io"
	"math"
)

var be = binary.BigEndian
var le = binary.LittleEndian

type GamePacketElemType uint8

const (
	GamePacketElemByte GamePacketElemType = 1 + iota
	GamePacketElemShort
	GamePacketElemInt
	GamePacketElemLong
	GamePacketElemFloat
	GamePacketElemString
	GamePacketElemBin
)

type gamePacket []iGamePacketElem

func newGamePacket(r io.Reader) (gamePacket, error) {
	b := make([]byte, 1)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	elemCount := int(b[0])
	l := make(gamePacket, 0, elemCount)

	// unused field
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	for i := 0; i < elemCount; i++ {
		if _, err := io.ReadFull(r, b); err != nil {
			return nil, err
		}

		switch t := GamePacketElemType(b[0]); t {
		case GamePacketElemByte:
			e, err := newGamePacketElemByte(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		case GamePacketElemShort:
			e, err := newGamePacketElemShort(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		case GamePacketElemInt:
			e, err := newGamePacketElemInt(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		case GamePacketElemLong:
			e, err := newGamePacketElemLong(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		case GamePacketElemFloat:
			e, err := newGamePacketElemFloat(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		case GamePacketElemString:
			e, err := newGamePacketElemString(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		case GamePacketElemBin:
			e, err := newGamePacketElemBin(r)
			if err != nil {
				return nil, err
			}

			l = append(l, e)

		default:
			return nil, fmt.Errorf("newGamePacket: unknown elem type %d %d", t, i)
		}
	}

	return l, nil
}

type iGamePacketElem interface {
	Type() GamePacketElemType
	Data() interface{}
}

var _ iGamePacketElem = (*gamePacketElemByte)(nil)

type gamePacketElemByte struct {
	value uint8
}

func (t *gamePacketElemByte) Type() GamePacketElemType {
	return GamePacketElemByte
}

func (t *gamePacketElemByte) Data() interface{} {
	return t.value
}

func newGamePacketElemByte(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 1)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	return &gamePacketElemByte{
		value: b[0],
	}, nil
}

type gamePacketElemShort struct {
	value uint16
}

func (t *gamePacketElemShort) Type() GamePacketElemType {
	return GamePacketElemShort
}

func (t *gamePacketElemShort) Data() interface{} {
	return t.value
}

func newGamePacketElemShort(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 2)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	return &gamePacketElemShort{
		value: be.Uint16(b),
	}, nil
}

type gamePacketElemInt struct {
	value uint32
}

func (t *gamePacketElemInt) Type() GamePacketElemType {
	return GamePacketElemInt
}

func (t *gamePacketElemInt) Data() interface{} {
	return t.value
}

func newGamePacketElemInt(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 4)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	return &gamePacketElemInt{
		value: be.Uint32(b),
	}, nil
}

type gamePacketElemLong struct {
	value uint64
}

func (t *gamePacketElemLong) Type() GamePacketElemType {
	return GamePacketElemLong
}

func (t *gamePacketElemLong) Data() interface{} {
	return t.value
}

func newGamePacketElemLong(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 8)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	return &gamePacketElemLong{
		value: be.Uint64(b),
	}, nil
}

type gamePacketElemFloat struct {
	value float32
}

func (t *gamePacketElemFloat) Type() GamePacketElemType {
	return GamePacketElemFloat
}

func (t *gamePacketElemFloat) Data() interface{} {
	return t.value
}

func newGamePacketElemFloat(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 4)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	return &gamePacketElemFloat{
		value: math.Float32frombits(be.Uint32(b)),
	}, nil
}

type gamePacketElemString struct {
	value string
}

func (t *gamePacketElemString) Type() GamePacketElemType {
	return GamePacketElemString
}

func (t *gamePacketElemString) Data() interface{} {
	return t.value
}

func newGamePacketElemString(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 2)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	length := be.Uint16(b)

	if length == 0 {
		// ?
		return &gamePacketElemString{
			value: "",
		}, nil
	}

	if length > 2 {
		b = make([]byte, length)
	}

	if _, err := io.ReadFull(r, b[:length]); err != nil {
		return nil, err
	}

	return &gamePacketElemString{
		// null termination
		value: string(b[:length-1]),
	}, nil
}

type gamePacketElemBin struct {
	value []byte
}

func (t *gamePacketElemBin) Type() GamePacketElemType {
	return GamePacketElemBin
}

func (t *gamePacketElemBin) Data() interface{} {
	return t.value
}

func newGamePacketElemBin(r io.Reader) (iGamePacketElem, error) {
	b := make([]byte, 2)
	if _, err := io.ReadFull(r, b); err != nil {
		return nil, err
	}

	length := be.Uint16(b)

	if length == 0 {
		// ?
		return &gamePacketElemBin{
			value: nil,
		}, nil
	}

	if length > 2 {
		b = make([]byte, length)
	}

	if _, err := io.ReadFull(r, b[:length]); err != nil {
		return nil, err
	}

	return &gamePacketElemBin{
		value: b,
	}, nil
}
