package main

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

type chatMsg struct {
	Nick   string
	Msg    string
	ImgUrl string
}

func (t *chatMsg) Send(s *discordgo.Session, chId string) error {
	// prevent mention
	msg := strings.ReplaceAll(t.Msg, "@", "at")

	// unescape
	msg = strings.ReplaceAll(msg, "&>", ">")
	msg = strings.ReplaceAll(msg, "&<", "<")

	req := &discordgo.MessageSend{
		Content: msg,
	}

	if t.Nick != "" {
		req.Content = t.Nick + ": " + req.Content
	}

	if t.ImgUrl != "" {
		req.Embeds = append(req.Embeds, &discordgo.MessageEmbed{
			Title: req.Content,
			Type:  discordgo.EmbedTypeImage,
			Image: &discordgo.MessageEmbedImage{
				URL: t.ImgUrl,
			},
		})
		req.Content = ""
	}

	if _, err := s.ChannelMessageSendComplex(chId, req); err != nil {
		return err
	}

	return nil
}
